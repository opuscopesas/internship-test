﻿using UnityEngine;

public static class GameObjectTools
{
    public static Bounds GetGlobalBounds(this GameObject gameObject)
    {
        Bounds bounds = default(Bounds);
        bool hasBeenInitialized = false;

        Collider[] colliders = gameObject.GetComponentsInChildren<Collider>();
        Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();

        for (int i = colliders.Length - 1; i > -1; i--)
        {
            if (!hasBeenInitialized)
            {
                bounds = colliders[i].bounds;
                hasBeenInitialized = true;
            } else
            {
                bounds.Encapsulate(colliders[i].bounds);
            }
        }

        for (int i = renderers.Length - 1; i > -1; i--)
        {
            if (!hasBeenInitialized)
            {
                bounds = renderers[i].bounds;
                hasBeenInitialized = true;
            }
            else
            {
                bounds.Encapsulate(renderers[i].bounds);
            }
        }

        return bounds;
    }
}
